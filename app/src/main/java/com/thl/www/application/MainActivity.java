package com.thl.www.application;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity {

    private WebView mWebView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
//        mWebView = (WebView) findViewById(R.id.WebView);
//        mWebView.getSettings().setUseWideViewPort(true);
//        mWebView.getSettings().setLoadWithOverviewMode(true);
//        mWebView.getSettings().setJavaScriptEnabled(true);
//        mWebView.getSettings().setBuiltInZoomControls(true);
//        mWebView.getSettings().setSupportZoom(true);
//        mWebView.getSettings().setDomStorageEnabled(true);
//        mWebView.setWebViewClient(new WebViewClient());
//        mWebView.getSettings().setAllowFileAccessFromFileURLs(true);
//        Map<String, String> extraHeaders = new HashMap<>();
//        extraHeaders.put("resource", "android");
//        mWebView.loadUrl("https://www.3shiyu.com/mallgoods/detail/93", extraHeaders);


        mWebView = (WebView) findViewById(R.id.WebView);
        WebSettings webSettings = mWebView.getSettings();
        webSettings.setJavaScriptEnabled(true); // Soutenir Javascript
        webSettings.setLoadWithOverviewMode(true);
        webSettings.setUseWideViewPort(true);

//        webSettings.setAllowFileAccess(true); // Autoriser WebView visiter des
//        webSettings.setBuiltInZoomControls(true);
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//            mWebView.getSettings().setMixedContentMode(
//                    WebSettings.MIXED_CONTENT_COMPATIBILITY_MODE);
//        }
//        mWebView.setWebViewClient(new WebViewClient() {
//            @Override
//            public boolean shouldOverrideUrlLoading(WebView view, String url) {
//                Log.e("taohaili", url);
//                if (url.startsWith("http:") || url.startsWith("https:")) {
//                    view.loadUrl(url);
//                    return false;
//                } else {
////                view.loadUrl(url);
//                    Toast.makeText(MainActivity.this, "yichang", Toast.LENGTH_SHORT).show();
//                    return false;
//                }
//            }
//
//            public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
//                handler.proceed();
//            }
//        });
        mWebView.loadUrl("file:///android_asset/demo.html");
//        mWebView.loadData(html,"text/html","UTF-8");
//        mWebView.loadUrl("https://www.baidu.com");
//        mWebView.loadData(html,"text/html; charset=UTF-8",null);
//        String url = "http://jade.jinhaidai.com/meet";
//        Log.e("taohaili","url:"+url);
//        Map<String,String> extraHeaders = new HashMap<>();
//        extraHeaders.put("resource", "android");
//        mWebView.loadUrl(url,extraHeaders);


        findViewById(R.id.clt_qq).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri parse = Uri.parse("mqqwpa://im/chat?chat_type=crm&uin=2852361293&version=1&src_type=web&web_src=http:://wpa.b.qq.com");
//                Uri parse = Uri.parse("mqqwpa://im/chat?chat_type=wpa&uin=" + "2852361293" + "&version=1");
                startActivity(new Intent(Intent.ACTION_VIEW, parse));

        }
        });

    }

    private class SampleWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            Log.e("taohaili", url);
            if (url.startsWith("http:") || url.startsWith("https:")) {
                view.loadUrl(url);
                return false;
            } else {
//                view.loadUrl(url);
                Toast.makeText(MainActivity.this, "yichang", Toast.LENGTH_SHORT).show();
                return false;
            }
        }
    }

//    @Override
//    public boolean onKeyDown(int keyCode, KeyEvent event) {
//        if (keyCode == KeyEvent.KEYCODE_BACK) {
//            mWebView.goBack();
//            return true;
//        }
//        return super.onKeyDown(keyCode, event);
//    }
}
